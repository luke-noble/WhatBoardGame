<?php

namespace App;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;

class BoardGame extends Model
{
    protected $fillable = [ 'name', 'min_players', 'max_players', 'min_playing_time', 'max_playing_time'];

    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'board_games.name' => 1
        ],
    ];
}
