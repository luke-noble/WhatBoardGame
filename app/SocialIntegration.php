<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialIntegration extends Model
{
    protected $fillable = [ 'user_id', 'provider', 'provider_id'];
    public $timestamps = false;

    public function Users()
    {
    	return $this->belongsTo('App\User');
    }
}
