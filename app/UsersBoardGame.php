<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersBoardGame extends Model
{
    protected $fillable = [ 'user_id', 'board_games_id'];
    public $timestamps = false;

    public function boardGames()
    {
    	return $this->belongsTo('App\BoardGame');
    }
}
