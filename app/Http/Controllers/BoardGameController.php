<?php

namespace App\Http\Controllers;

use App\BoardGame;
use Illuminate\Http\Request;

class BoardGameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function addGame()
    {
    	return view('add-game.addgame');
    }

    public function find(Request $request)
    {
        return BoardGame::search($request->get('q'))->get();
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
        	'name' => 'required|unique:board_games|max:255',
        	'min_players' => 'required|integer',
        	'max_players' => 'required|integer|greater_than_field:min_players',
        	'min_playing_time' => 'required|integer',
        	'max_playing_time' => 'required|integer|greater_than_field:min_playing_time',
    	]);
        
    	BoardGame::create([
    		'name' => request('name'),
    		'min_players' => request('min_players'),
    		'max_players' => request('max_players'),
    		'min_playing_time' => request('min_playing_time'),
    		'max_playing_time' => request('max_playing_time'),
    	]);

    	return view('add-game.gameadded');
    }
}
