<?php

namespace App\Http\Controllers;

use App\BoardGame;
use App\UsersBoardGame;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class UsersBoardGameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function search()
    {
        return view('search');
    }

    public function store(Request $request)
    {
    	UsersBoardGame::firstOrCreate([
    		'user_id' => auth()->id(),
    		'board_games_id' => request('id')
    		]);
    	return view('add-game.gameadded');
    }

    public function show()
    {
        $myBoardGames = UsersBoardGame::where('user_id', auth()->id())->get();
        return view('collection', compact('myBoardGames'));
    }

    public function destroy($boardGameId)
    {
        UsersBoardGame::where([
            ['user_id', '=' ,auth()->id() ],
            ['board_games_id', '=', $boardGameId]
            ])->delete();

        return redirect('/my-collection');
    }

    public function pickGame(Request $request)
    {
        $usersGames = DB::table('users_board_games')
                               ->join('board_games', 'users_board_games.board_games_id', '=', 'board_games.id')
                               ->where([
                                    ['user_id', '=', auth()->id()],
                                    ['min_players', '<=', request('num_of_players')],
                                    ['max_players', '>=', request('num_of_players')],
                                    ['min_playing_time', '<=', request('time')],
                                    ['max_playing_time', '>=', request('time')],
                                ])
                               ->inRandomOrder()
                               ->get();
        if ($usersGames->isEmpty())
        {
            return view('no-games');
        }
        return view('pick-game', compact('usersGames'));
    }
}
