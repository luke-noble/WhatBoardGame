<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/add-game', 'BoardGameController@addGame');
Route::post('/add-game', 'BoardGameController@store');
Route::get('/game-search', 'UsersBoardGameController@search');
Route::post('/game-search', 'UsersBoardGameController@store');
Route::get('find', 'BoardGameController@find');
Route::get('/my-collection', 'UsersBoardGameController@show');
Route::post('/delete-game/{board_game_id}', 'UsersBoardGameController@destroy');
Route::post('/pick-game', 'UsersBoardGameController@pickGame');


Route::get('/home', 'HomeController@index')->name('home');

//Social login routes
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
