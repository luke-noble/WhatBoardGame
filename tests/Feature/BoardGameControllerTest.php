<?php

namespace Tests\Feature;

use App\BoardGame;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BoardGameControllerTest extends TestCase
{
	use DatabaseMigrations;

    public function testAddGame()
    {
        $user = factory(\App\User::class)->make();
    	$response = $this->actingAs($user)->get('/add-game');
    	$response->assertStatus(200)
    			 ->assertSee('Name:');
    }

    public function testStore()
    {
        $user = factory(\App\User::class)->make();
    	$game = factory(\App\BoardGame::class)->make();
    	$response = $this->actingAs($user)->post("/add-game", array(
            '_token' => csrf_token(),
            'name' => $game->name,
            'min_players' =>$game->min_players,
            'max_players' => $game->max_players,
            'min_playing_time' => $game->min_playing_time,
            'max_playing_time' => $game->max_playing_time
        ));
        $savedGame = BoardGame::first();
        $this->assertTrue($savedGame->name == $game->name);
        $this->assertTrue($savedGame->min_players == $game->min_players);
        $this->assertTrue($savedGame->max_players == $game->max_players);
        $this->assertTrue($savedGame->min_playing_time == $game->min_playing_time);
        $this->assertTrue($savedGame->max_playing_time == $game->max_playing_time);
        $response->assertSee('Your game has successfully been added.');
    }
}
