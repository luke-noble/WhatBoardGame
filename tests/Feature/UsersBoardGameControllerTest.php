<?php

namespace Tests\Feature;

use App\User;
use App\UsersBoardGame;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UsersBoardGameControllerTest extends TestCase
{
	use DatabaseMigrations;

    public function testSearch()
    {
        $user = factory(\App\User::class)->make();
    	$response = $this->actingAs($user)->get('/game-search');
    	$response->assertStatus(200)
    			 ->assertSee('<form method="post" action="/game-search">');
    }

    public function testStore()
    {
    	$user = factory(\App\User::class)->create();
    	$games = factory(\App\BoardGame::class, 3)->create();
    	$response = $this->actingAs($user)->post('/game-search', array(
    		'id' => 2
    	));
    	$userGame = UsersBoardGame::first();
    	$this->assertTrue($userGame->user_id == 1);
    	$this->assertTrue($userGame->board_games_id == 2);
    }

    public function testShow()
    {
        $user = factory(\App\User::class)->create();
        $game = factory(\App\BoardGame::class)->create();
        $userGame = factory(\App\UsersBoardGame::class)->create();
        $response = $this->actingAs($user)->get('/my-collection');
        $response->assertStatus(200)
                 ->assertSee($game['name']);
    }

    public function testShowWhenEmpty()
    {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)->get('/my-collection');
        $response->assertStatus(200)
                 ->assertSee('You have no games in your collection');
    }

    public function testDestroy()
    {
        $user = factory(\App\User::class)->create();
        $game = factory(\App\BoardGame::class)->create();
        $userGame = factory(\App\UsersBoardGame::class)->create();
        $response = $this->actingAs($user)->post('/delete-game/1', array(
            'delete' => 1
        ));
        $testCase = UsersBoardGame::all()->count();
        $this->assertTrue($testCase == 0);
        $response->assertStatus(302);
    }

    public function testPickGame()
    {
        $user = factory(\App\User::class)->create();
        $game = factory(\App\BoardGame::class)->create();
        $userGame = factory(\App\UsersBoardGame::class)->create();
        $response = $this->actingAs($user)->post('/pick-game', array(
            'num_of_players' => 3,
            'time' => 15
        ));
        $response->assertStatus(200)
                 ->assertSee($game->name);   
    }

    public function testPickGameFail()
    {
        $user = factory(\App\User::class)->create();
        $game = factory(\App\BoardGame::class)->create();
        $userGame = factory(\App\UsersBoardGame::class)->create();
        $response = $this->actingAs($user)->post('/pick-game', array(
            'num_of_players' => 10,
            'time' => 999
        ));
        $response->assertStatus(200)
                 ->assertSee("Unfortunately there are no games in your collection that are suitable!"); 
    }
}
