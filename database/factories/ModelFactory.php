<?php
use App\User;
use App\BoardGame;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\BoardGame::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'min_players' => $faker->numberBetween($min=1, $max=3),
        'max_players' => $faker->numberBetween($min=4, $max=9),
        'min_playing_time' => $faker->numberBetween($min=1, $max=15),
        'max_playing_time' => $faker->numberBetween($min=16, $max=120),
    ];
});

// Must have users and board games created already to work!
$factory->define(App\UsersBoardGame::class, function (Faker\Generator $faker) {
    return [
        'user_id' => User::all()->random()->id,
        'board_games_id' => BoardGame::all()->random()->id,
    ];
});
