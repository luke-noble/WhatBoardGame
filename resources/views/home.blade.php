<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Home</title>

    </head>
    <body>
    <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
        <form method="post" action="/pick-game">
        {{ csrf_field() }}
        <div class="form-group">
            @if ($errors->has('num-of-players'))
                <strong>{{ $errors->first('num-of-players') }}</strong>
            @endif
                <label>Number of players:</label>
                <input type="text" name="num_of_players" required>
        </div>
        <div class="form-group">
            @if ($errors->has('time'))
                <strong>{{ $errors->first('time') }}</strong>
            @endif
                <label>Time:</label>
                <input type="text" name="time" required>
        </div>
        <input type="submit">

        </form>
    </body>

</html>
