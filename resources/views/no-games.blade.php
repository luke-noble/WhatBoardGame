<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>No suitable games!</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

	</head>
	<body>
		<h1>Unfortunately there are no games in your collection that are suitable!</h1>
	</body>

</html>