<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Add Game</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

	</head>
	<body>
	@if ($myBoardGames->count() < 1)
		<span>You have no games in your collection</span>
	@endif
		<ul>
			@foreach ($myBoardGames as $boardGame)
				<li>{{$boardGame->boardGames['name']}}</li> <form method="post" action="/delete-game/{{$boardGame['board_games_id']}}"">{{csrf_field()}}<input type="submit" value="delete"></form>
			@endforeach
		</ul>
	</body>

</html>