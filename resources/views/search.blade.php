<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Add Game</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

	</head>
	<body>
		<div>Search time</div>
		<form class="typeahead" role="search">
      		<div class="">
        	   <input type="search" name="q" class="search-input" placeholder="Search" id="search-box" autocomplete="off">
      		</div>
    	</form>

        <form method="post" action="/game-search">
            {{ csrf_field() }}
            <div class="form-group">
                @if ($errors->has('name'))
                    <strong>{{ $errors->first('name') }}</strong>
                @endif
                <label>Name:</label>
                <input type="text" name="name" id="name" required>
            </div>
            <input type="hidden" name="id" id="game-id" value="">
            <input type="submit" value="Add game to collection">
        </form>
	</body>
	<script src="{{ asset('js/app.js') }}"></script>
	<script src="{{ asset('js/game-search.js') }}"></script>

</html>