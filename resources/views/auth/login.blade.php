@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <a href="{{ url('/auth/twitter') }}" class="social-link"><img src="{{ asset('img/twitter-sign-in.png')}}" class="twitter-link"></a><br>
                    <a href="{{ url('/auth/facebook') }}" class="social-link"><img src="{{ asset('img/facebook-sign-in.png') }}" class="facebook-link"></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
