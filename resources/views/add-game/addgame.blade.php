<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Add Game</title>
	</head>
	<body>
		<form method="post" action="/add-game">
			{{ csrf_field() }}
			<div class="form-group">
				@if ($errors->has('name'))
                    <strong>{{ $errors->first('name') }}</strong><br>
                @endif
				<label>Name:</label>
				<input type="text" name="name" required>
			</div>

			<div class="form-group">
				@if ($errors->has('min_players'))
                    <strong>{{ $errors->first('min_players') }}</strong><br>
                @endif
				<label>Min players:</label>
				<input type="text" name="min_players" required>
			</div>

			<div class="form-group">
				@if ($errors->has('max_players'))
                    <strong>{{ $errors->first('max_players') }}</strong><br>
                @endif
				<label>Max players:</label>
				<input type="text" name="max_players" required>
			</div>

			<div class="form-group">
				@if ($errors->has('min_playing_time'))
                    <strong>{{ $errors->first('min_playing_time') }}</strong><br>
                @endif
                @if ($errors->has('max_playing_time'))
                    <strong>{{ $errors->first('max_playing_time') }}</strong><br>
                @endif
				<label>Playing time:</label>
				<input type="text" name="min_playing_time" required> - <input type="text" name="max_playing_time" required> mins
			</div>

			<input type="submit">
		</form>
	</body>
</html>