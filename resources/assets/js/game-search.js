jQuery(document).ready(function($) {
    // Set the Options for "Bloodhound" suggestion engine
    var engine = new Bloodhound({
        remote: {
            url: '/find?q=%QUERY%',
            wildcard: '%QUERY%'
        },
        datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });

    $(".search-input").typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        source: engine.ttAdapter(),

        // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
        name: 'boardGames',

        // the key from the array we want to display (name,id,email,etc...)
        templates: {
            empty: [
                '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
            ],
            header: [
                '<div class="list-group search-results-dropdown">'
            ],
            suggestion: function(data) {
                return '<div class="list-group-item" id="test-id">' + data.name + '</div>'
            }
        }
    });

    $('.search-input').bind('typeahead:select', function(ev, suggestion) {
        document.getElementById('search-box').value = "";
        updateTextInput('name', suggestion.name);
        updateTextInput('game-id', suggestion.id);
    });


    function updateTextInput(fieldID, fieldText) {
        document.getElementById(fieldID).value = fieldText
    };

});